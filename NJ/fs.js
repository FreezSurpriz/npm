const fs = require('fs');
const path = require('path');

fs.mkdir(path.join(__dirname, 'notes',), err => {
    if (err) throw err

    console.log('создан')
})

fs.writeFile(
    path.join(__dirname, 'notes', 'mynotes.txt'),
    'Привет',
    (err) => {
        if (err) throw err
        console.log('Файл создан')

        fs.appendFile(
            path.join(__dirname, 'notes', 'mynotes.txt'),
            ' Из добавляемого файла',
            err => {
                if (err) throw err
                console.log('Файл изменен')

                fs.readFile(
                    path.join(__dirname, 'notes', 'mynotes.txt'),
                    'utf-8',
                    (err,data) => {
                        if (err) throw err
                        console.log(data)
                    }
                )
            }
        )
    }
)

fs.rename(
    path.join(__dirname, 'notes', 'mynotes.txt'),
    path.join(__dirname, 'notes', 'notes.txt'),
    err => {
        if (err) throw err

        console.log('Файл переименован')
    }
)

